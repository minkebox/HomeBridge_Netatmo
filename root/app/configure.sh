#! /bin/sh

cat /app/config.json | sed \
  -e "s/{{BRIDGE_USERNAME}}/${BRIDGE_USERNAME}/g" \
  -e "s/{{BRIDGE_PIN}}/${BRIDGE_PIN}/g" \
  -e "s/{{BRIDGE_SETUPID}}/${BRIDGE_SETUPID}/g" \
  -e "s/{{CLIENT_ID}}/${CLIENT_ID}/g" \
  -e "s/{{CLIENT_SECRET}}/${CLIENT_SECRET}/g" \
  -e "s/{{USERNAME}}/${USERNAME}/g" \
  -e "s/{{PASSWORD}}/${PASSWORD}/g" \
  > /app/homebridge/config.json
